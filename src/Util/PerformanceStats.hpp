// 
// Global counters used to track performance
// Mostly copied from CUDAcommon.h


#ifndef MEDYAN_Util_PerformanceStats_Hpp
#define MEDYAN_Util_PerformanceStats_Hpp

#include "common.h"
#include "Chemistry/ChemTypes.hpp"

namespace medyan {


struct PerformanceStats {

    struct ChemDetails {
        int64_t reactioncount[numReactionTypes] = {};

        // Callbacks.
        Size cblinkerbinding = 0;
        Size cblinkerunbinding = 0;
        Size cbmotorwalking = 0;
    };
    
    struct MechDetails{
        int64_t numEnergyCall = 0;
        int64_t numForceCall = 0;
    };

    MechDetails mdetails;

    ChemDetails cdetails;
 
// TODO    inline mechdetails mdetails;


    /**
     * log all counts, except callback related counts, with log::info.
     */  
    void logStats() const;
};

} // namespace medyan

#endif

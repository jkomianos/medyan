#include "Util/PerformanceStats.hpp"
#include "Util/Io/Log.hpp"

namespace medyan {

void PerformanceStats::logStats() const {
    // First log the chem details
    log::info("Reaction count per type: ");
    // Get the max strlen of text(ReactionType(i)) strings
    int max_strlen = 0;
    for (int i = 0; i < numReactionTypes; i++) {
        int length = strlen(text(ReactionType(i)));
        if (length > max_strlen) {
            max_strlen = length;
        }
    }
    // Go through ReactionType enum class options and print the name
    for (int i=0; i<numReactionTypes; i++) {
        if (cdetails.reactioncount[i] == 0) {
            continue;
        }
        int length = strlen(text(ReactionType(i)));
        int extraspaces = max_strlen - length;
        std::string extrastringspaces(extraspaces, ' ');
        log::info("    {}{}: {}", text(ReactionType(i)), extrastringspaces, cdetails.reactioncount[i]);
    }
    // Now log the mech details
    log::info("Energy call count: {}", mdetails.numEnergyCall);
    log::info("Force call count: {}", mdetails.numForceCall);
}

} // namespace medyan

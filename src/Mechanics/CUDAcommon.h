//
// Created by aravind on 9/18/17.
//

#ifndef MEDYAN_CUDAcommon_h
#define MEDYAN_CUDAcommon_h

#include <vector>
#include <list>

#include "FilamentStretchingHarmonic.h"
#include "FilamentBendingHarmonic.h"
#include "FilamentBendingCosine.h"
#include "LinkerStretchingHarmonic.h"
#include "MotorGhostStretchingHarmonic.h"
#include "CylinderExclVolRepulsion.h"
#include "BranchingStretchingHarmonic.h"
#include "BranchingBendingCosine.h"
#include "BranchingDihedralCosine.h"
#include "BranchingPositionCosine.h"
#include "BoundaryCylinderRepulsionExp.h"
#include "CCylinder.h"
#include "common.h"
#include "string.h"
#include "MathFunctions.h"
#include "Structure/Bead.h"
#ifdef SIMDBINDINGSEARCH
#include "Util/DistModule/dist_driver.h"

namespace medyan {
template <uint D>
dist::dOut<D> SIMDoutvar(const uint dim, uint N1, std::initializer_list<float> params) {

    if (dim == 1) {
        dist::dOut<1> out_serialdim(N1, params);
        return out_serialdim;
    }
    else if (dim == 2) {
        dist::dOut<2> out_serialdim(N1, params);
        return out_serialdim;
    }
    else if (dim == 3){
        dist::dOut<3> out_serialdim(N1, params);
        return out_serialdim;
    }
}
#endif

using namespace mathfunc;
struct bin{
    int binID;
    floatingpoint bincoord[3];
    int neighbors[27] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
                          -1,-1,-1,-1,-1,-1};
    int binstencilID[27] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
                          -1,-1,-1,-1,-1,-1};
};

struct Callbacktime {
	floatingpoint tUpdateBrancherBindingCallback=0.0;
	floatingpoint tUpdateLinkerBindingCallback=0.0;
	floatingpoint tUpdateMotorBindingCallback=0.0;
	floatingpoint tUpdateMotorIDCallback=0.0;
	floatingpoint tFilamentExtensionPlusEndCallback=0.0;
	floatingpoint tFilamentExtensionMinusEndCallback=0.0;
	floatingpoint tFilamentRetractionPlusEndCallback=0.0;
	floatingpoint tFilamentRetractionMinusEndCallback=0.0;
	floatingpoint tFilamentPolymerizationPlusEndCallback=0.0;
	floatingpoint tFilamentPolymerizationMinusEndCallback=0.0;
	floatingpoint tFilamentDepolymerizationPlusEndCallback=0.0;
	floatingpoint tFilamentDepolymerizationMinusEndCallback=0.0;
	floatingpoint tBranchingPointUnbindingCallback=0.0;
	floatingpoint tBranchingCallback=0.0;
	floatingpoint tLinkerUnbindingCallback=0.0;
	floatingpoint tLinkerBindingCallback=0.0;
	floatingpoint tMotorUnbindingCallback=0.0;
	floatingpoint tMotorBindingCallback=0.0;
	floatingpoint tMotorWalkingCallback=0.0;
	floatingpoint tMotorMovingCylinderCallback=0.0;
	floatingpoint tFilamentCreationCallback=0.0;
	floatingpoint tFilamentSeveringCallback=0.0;
	floatingpoint tFilamentDestructionCallback=0.0;
};

struct Callbackcount {
	uint cUpdateBrancherBindingCallback=0;
	uint cUpdateLinkerBindingCallback=0;
	uint cUpdateMotorBindingCallback=0;
	uint cUpdateMotorIDCallback=0;
	uint cFilamentExtensionPlusEndCallback=0;
	uint cFilamentExtensionMinusEndCallback=0;
	uint cFilamentRetractionPlusEndCallback=0;
	uint cFilamentRetractionMinusEndCallback=0;
	uint cFilamentPolymerizationPlusEndCallback=0;
	uint cFilamentPolymerizationMinusEndCallback=0;
	uint cFilamentDepolymerizationPlusEndCallback=0;
	uint cFilamentDepolymerizationMinusEndCallback=0;
	uint cBranchingPointUnbindingCallback=0;
	uint cBranchingCallback=0;
	uint cLinkerUnbindingCallback=0;
	uint cLinkerBindingCallback=0;
	uint cMotorUnbindingCallback=0;
	uint cMotorBindingCallback=0;
	uint cMotorWalkingCallback=0;
	uint cMotorMovingCylinderCallback=0;
	uint cFilamentCreationCallback=0;
	uint cFilamentSeveringCallback=0;
	uint cFilamentDestructionCallback=0;
};


struct PolyPlusEndTemplatetime{
	floatingpoint rxntempate1 = 0.0;
	floatingpoint rxntempate2 = 0.0;
	floatingpoint rxntempate3 = 0.0;
	floatingpoint rxntempate4 = 0.0;

};

struct timeminimization{
	floatingpoint vectorize = 0.0;
	floatingpoint findlambda = 0.0;
	floatingpoint computeforces = 0.0;
	floatingpoint computeenergy = 0.0;
	floatingpoint computeenergyzero = 0.0;
	floatingpoint computeenergynonzero = 0.0;
	vector<floatingpoint> individualenergies;
	vector<floatingpoint> individualenergieszero;
	vector<floatingpoint> individualenergiesnonzero;
	vector<floatingpoint> individualforces;
	floatingpoint endminimization = 0.0;
	floatingpoint tother = 0.0;
	floatingpoint copyforces =0.0;
	floatingpoint stretchingenergy= 0.0;
	floatingpoint stretchingforces= 0.0;
	floatingpoint bendingenergy = 0.0;
	floatingpoint bendingforces = 0.0;
	int computeenergycalls = 0;
	int computeenerycallszero = 0;
	int computeenerycallsnonzero = 0;
	int computeforcescalls = 0;
	int numinteractions[10] = {0,0,0,0,0,0,0,0,0,0};
	floatingpoint timecylinderupdate = 0.0;
	floatingpoint timelinkerupdate = 0.0;
	floatingpoint timemotorupdate = 0.0;
	int callscylinderupdate = 0;
	int callslinkerupdate = 0;
	int callsmotorupdate = 0;
	//
	int motorbindingcalls = 0;
	int motorunbindingcalls = 0;
	int linkerbindingcalls = 0;
	int linkerunbindingcalls = 0;
	int motorwalkingcalls =0;

};


class CUDAcommon{
public:
    static Callbacktime ctime;
	static Callbackcount ccount;
	static PolyPlusEndTemplatetime ppendtime;
	static timeminimization tmin;
};

} // namespace medyan

#endif
//CUDA_VEC_CUDACOMMON_H

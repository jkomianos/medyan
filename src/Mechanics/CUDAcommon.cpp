//
// Created by aravind on 9/19/17.
//

#include "CUDAcommon.h"

namespace medyan {

Callbacktime CUDAcommon::ctime;
Callbackcount CUDAcommon::ccount;
PolyPlusEndTemplatetime CUDAcommon::ppendtime;
timeminimization CUDAcommon::tmin;

} // namespace medyan

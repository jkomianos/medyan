
//------------------------------------------------------------------
//  **MEDYAN** - Simulation Package for the Mechanochemical
//               Dynamics of Active Networks, v4.0
//
//  Copyright (2015-2018)  Papoian Lab, University of Maryland
//
//                 ALL RIGHTS RESERVED
//
//  See the MEDYAN web page for more information:
//  http://www.medyan.org
//------------------------------------------------------------------

#ifndef MEDYAN_ChemSimpleGillespieImpl_h
#define MEDYAN_ChemSimpleGillespieImpl_h

#include <vector>
#include <random>

#include "common.h"

#include "Reaction.h"
#include "ChemRNode.h"
#include "Chemistry/DissipationTracker.h"
#include "Util/PerformanceStats.hpp"

namespace medyan {

/// Implements the simplest version of the Gillespie algorithm, without caching, etc.

/*! 
 *  ChemSimpleGillespieImpl manages the Gillespie algorithm at the level of the network
 *  of reactions. Reaction objects can be added and removed from the 
 *  ChemSimpleGillespieImpl instance.
 */
class ChemSimpleGillespieImpl {
public:
    /// Ctor: Seeds the random number generator, sets global time to 0.0
    ChemSimpleGillespieImpl() = default;
    
    /// Copying is not allowed
    ChemSimpleGillespieImpl(const ChemSimpleGillespieImpl &rhs) = delete;
    
    /// Assignment is not allowed
    ChemSimpleGillespieImpl& operator=(ChemSimpleGillespieImpl &rhs) = delete;

    ///Dtor: The reaction network is cleared.
    /// @note noexcept is important here. Otherwise, gcc flags the constructor as
    /// potentially throwing, which in turn disables move operations by the STL
    /// containers. This behaviour is a gcc bug (as of gcc 4.703), and will presumbaly
    /// be fixed in the future.
    ~ChemSimpleGillespieImpl() noexcept;
    
    /// Return the number of reactions in the network.
    size_t getSize() const {return _reactions.size();}
    
    /// Add ReactionBase *r to the network
    void addReaction(ReactionBase *r);
    
    /// Remove ReactionBase *r from the network
    void removeReaction(ReactionBase *r);

    
    /// Compute the total propensity of the reaction network, by adding all individual
    /// reaction propensities
    floatingpoint computeTotalA();
    
    /// A pure function (without sideeffects), which returns a random time tau, drawn
    /// from the exponential distribution, with the propensity given by a.
    floatingpoint generateTau(floatingpoint a);
    
    /// This function generates a random number between 0 and 1
    floatingpoint generateUniform();
    
    /// This function needs to be called before calling run(...).
    /// @note If somewhere in the middle of simulaiton initialize() is called, it will
    /// be analogous to starting the simulation from scratch, except with the Species
    /// copy numbers given at that moment in time. The global time is reset to zero again.
    void initialize();
    
    /// Prints all Reaction objects in the reaction network
    void printReactions() const;

    /// Cross checks all reactions in the network for firing time.
    bool crosschecktau() const {
        log::warn("Cannot check for tau in reactions in ChemGillespieImpl.h");
        return true;
    };
    
    /// This subroutine implements the vanilla version of the Gillespie algorithm
    bool makeStep(
        FP& time,
        FP endTime,
        DissipationTracker* pdt = nullptr,
        PerformanceStats* pstats = nullptr
    ) {
        floatingpoint a_total = computeTotalA();

        floatingpoint tau = generateTau(a_total);
        // Check if a reaction happened before endTime
        if (time+tau>endTime){ 
            time = endTime;
            return true;
        }
        // this means that the network has come to a halt
        if(a_total<1e-15)
            return false;
        time+=tau;
        
        //Gillespie algorithm's second step: finding which reaction happened;
        floatingpoint mu = a_total*generateUniform();
        floatingpoint rates_sum = 0;
        ReactionBase* r_selected = nullptr;
        for (auto &r : _reactions){
            
            rates_sum+=r->computePropensity();
        
            if(rates_sum>mu){
                r_selected = r;
                break;
            }
        }
        if(r_selected==nullptr){
            cout << "ChemSimpleGillespieImpl::makeStep() for loop: rates_sum=" <<
                rates_sum << ", mu="
                << mu << ", a_total=" << a_total << endl;
            throw runtime_error( "ChemSimpleGillespieImpl::makeStep(): No Reaction was selected during the Gillespie step!");
        }
        r_selected->makeStep();
        if(pstats) {
            ++pstats->cdetails.reactioncount[underlying(r_selected->getReactionType())];
        }

        // Send signal.
        r_selected->emitSignal();
        
        return true;
    }

private:
    vector<ReactionBase*> _reactions; ///< The database of Reaction objects,
                                      ///< representing the reaction network
    exponential_distribution<floatingpoint> _exp_distr; ///< Adaptor for the exponential distribution
    uniform_real_distribution<floatingpoint> _uniform_distr; ///< Adaptor for the uniform distribution
};

} // namespace medyan

#endif

#ifndef MEDYAN_Chemistry_ChemTypes_hpp
#define MEDYAN_Chemistry_ChemTypes_hpp

#include "utility.h"

namespace medyan {

enum class SpeciesType {
    unspecified,
    general, BULK, DIFFUSING, FILAMENT, BOUND, LINKER, MOTOR, BRANCHER, PLUSEND, MINUSEND,
    singleBinding, pairBinding,
    // Last item indicating number of elements.
    last_
};

constexpr const char* speciesSuffix(SpeciesType type) {
    switch(type) {
        case SpeciesType::general:     return "{General}";
        case SpeciesType::BULK:        return "{Bulk}";
        case SpeciesType::DIFFUSING:   return "{Diffusing}";
        case SpeciesType::FILAMENT:    return "{Filament}";
        case SpeciesType::BOUND:       return "{Bound}";
        case SpeciesType::LINKER:      return "{Linker}";
        case SpeciesType::MOTOR:       return "{Motor}";
        case SpeciesType::BRANCHER:    return "{Brancher}";
        case SpeciesType::PLUSEND:     return "{PlusEnd}";
        case SpeciesType::MINUSEND:    return "{MinusEnd}";
        case SpeciesType::singleBinding: return "{SingleBinding}";
        case SpeciesType::pairBinding: return "{PairBinding}";
        default:                       return "{None}";
    }
}
inline constexpr auto numSpeciesTypes = underlying(SpeciesType::last_);


enum class ReactionType {
    unspecified,
    REGULAR, 
    DIFFUSION,
    emission, 
    absorption,
    POLYMERIZATIONPLUSEND, 
    POLYMERIZATIONMINUSEND,
    DEPOLYMERIZATIONPLUSEND, 
    DEPOLYMERIZATIONMINUSEND,
    LINKERBINDING, 
    MOTORBINDING, 
    LINKERUNBINDING, 
    MOTORUNBINDING,
    MOTORWALKINGFORWARD, 
    MOTORWALKINGBACKWARD,
    AGING, 
    FILAMENTCREATION, 
    FILAMENTDESTRUCTION,
    BRANCHING, 
    BRANCHUNBINDING, 
    SEVERING,
    membraneDiffusion,
    membraneAdsorption, membraneDesorption,
    // Last item indicating number of elements.
    last_
};

constexpr const char* text(ReactionType t) {
    switch(t) {
        case ReactionType::REGULAR:                  return "regular";
        case ReactionType::DIFFUSION:                return "diffusion";
        case ReactionType::emission:                 return "emission";
        case ReactionType::absorption:               return "absorption";
        case ReactionType::POLYMERIZATIONPLUSEND:    return "polymerizationPlusEnd";
        case ReactionType::POLYMERIZATIONMINUSEND:   return "polymerizationMinusEnd";
        case ReactionType::DEPOLYMERIZATIONPLUSEND:  return "depolymerizationPlusEnd";
        case ReactionType::DEPOLYMERIZATIONMINUSEND: return "depolymerizationMinusEnd";
        case ReactionType::LINKERBINDING:            return "linkerBinding";
        case ReactionType::MOTORBINDING:             return "motorBinding";
        case ReactionType::LINKERUNBINDING:          return "linkerUnbinding";
        case ReactionType::MOTORUNBINDING:           return "motorUnbinding";
        case ReactionType::MOTORWALKINGFORWARD:      return "motorWalkingForward";
        case ReactionType::MOTORWALKINGBACKWARD:     return "motorWalkingBackward";
        case ReactionType::AGING:                    return "aging";
        case ReactionType::FILAMENTCREATION:         return "filamentCreation";
        case ReactionType::FILAMENTDESTRUCTION:      return "filamentDestruction";
        case ReactionType::BRANCHING:                return "branching";
        case ReactionType::BRANCHUNBINDING:          return "branchUnbinding";
        case ReactionType::SEVERING:                 return "severing";
        case ReactionType::membraneDiffusion:        return "membraneDiffusion";
        case ReactionType::membraneAdsorption:       return "membraneAdsorption";
        case ReactionType::membraneDesorption:       return "membraneDesorption";
        default:                                     return "unspecified";
    }
}
inline constexpr auto numReactionTypes = underlying(ReactionType::last_);

} // namespace medyan

#endif


//------------------------------------------------------------------
//  **MEDYAN** - Simulation Package for the Mechanochemical
//               Dynamics of Active Networks, v4.0
//
//  Copyright (2015-2018)  Papoian Lab, University of Maryland
//
//                 ALL RIGHTS RESERVED
//
//  See the MEDYAN web page for more information:
//  http://www.medyan.org
//------------------------------------------------------------------

#ifndef MEDYAN_ChemSim_h
#define MEDYAN_ChemSim_h

#include <type_traits>
#include <variant>

#include "common.h"
#include "Chemistry/ChemGillespieImpl.h"
#include "Chemistry/ChemNRMImpl.h"
#include "Chemistry/ChemSimpleGillespieImpl.h"
#include "Chemistry/DissipationTracker.h"
    
namespace medyan {
//FORWARD DECLARATIONS
class ReactionBase;
class DissipationTracker;

using ChemSimImpl = std::variant< ChemSimpleGillespieImpl, ChemGillespieImpl, ChemNRMImpl >;
template< typename T >
inline constexpr bool isChemSimImpl =
    std::is_same_v< T, ChemSimpleGillespieImpl > ||
    std::is_same_v< T, ChemGillespieImpl > ||
    std::is_same_v< T, ChemNRMImpl >;


// Run the chemical dynamics for a set amount of time.
// Returns true if all steps are successful.
template< typename T, std::enable_if_t< isChemSimImpl<T> >* = nullptr >
inline bool run(T& cs, FP& curtime, FP time, DissipationTracker* pdt=nullptr, PerformanceStats* pstats=nullptr) {
    auto endTime = curtime + time;

    while(curtime < endTime) {
        bool success = cs.makeStep(curtime, endTime, pdt, pstats);
        if(!success)
            return false;
    }
    return true;
}

// Run the chemical dynamics for a set amount of reaction steps.
// Returns true if all steps are successful.
template< typename T, std::enable_if_t< isChemSimImpl<T> >* = nullptr >
inline bool runSteps(T& cs, FP& curtime, int steps, DissipationTracker* pdt=nullptr, PerformanceStats* pstats=nullptr) {
    for(int i = 0; i < steps; i++) {
        bool success = cs.makeStep(curtime, inffp, pdt, pstats);
        if(!success) {
            return false;
        }
    }
    return true;
}



template< typename... Args >
inline bool run(ChemSimImpl& cs, Args&&... args) {
    return std::visit(
        [&](auto& c) { return run(c, std::forward<Args>(args)...); },
        cs
    );
}

template< typename... Args >
inline bool runSteps(ChemSimImpl& cs, Args&&... args) {
    return std::visit(
        [&](auto& c) { return runSteps(c, std::forward<Args>(args)...); },
        cs
    );
}

// After all initial reactions have been added via addReaction(...) method,
// invoke initialize() prior to invoking run().
inline void initialize(ChemSimImpl& cs) {
    return std::visit(
        [&](auto& c) { c.initialize(); },
        cs
    );
}

// Necessary to call during restart to reset time to necessary time.
inline void setTime(ChemSimImpl& cs, FP time) {
    return std::visit(
        Overload {
            [&](ChemNRMImpl& c) { c.setTime(time); },
            [](auto&) {},
        },
        cs
    );
}

// Add a reaction to the chemical network.
inline void addReaction(ChemSimImpl& cs, ReactionBase* r) {
    return std::visit(
        [&](auto& c) { c.addReaction(r); },
        cs
    );
}
// Remove a reaction from the chemical network.
inline void removeReaction(ChemSimImpl& cs, ReactionBase* r) {
    return std::visit(
        [&](auto& c) { c.removeReaction(r); },
        cs
    );
}

// Print chemical reactions in the network.
inline void printReactions(const ChemSimImpl& cs) {
    return std::visit(
        [&](auto& c) { c.printReactions(); },
        cs
    );
}

// Cross check all reactions in the network for firing time.
inline bool crosschecktau(const ChemSimImpl& cs) {
    return std::visit(
        [&](auto& c) { return c.crosschecktau(); },
        cs
    );
}



/// Used to manage running a network of chemical reactions.

/*! ChemSim implements a Strategy pattern, allowing custom algorithms for running 
 *  stochastic chemical simulations. It itself has a pointer to a  single static 
 *  implementation of ChemSimImpl. After the specific algorithm is chosen and ChemSim 
 *  is instantiated, ChemSim can be used to manage simulations, through such methods 
 *  as run(steps) etc.
 */
// Old ChemSim interface for backward compatibility.
// Use functions above instead.
struct ChemSim {
public:
    ChemSimImpl impl;
    
    /// After all initial reactions have been added via addReaction(...) method,
    /// invoke initialize() prior to invoking run()
    void initialize() { medyan::initialize(impl); }

    //necessary to call during restart to reset global time to necessary time.
    void setTime(FP time) { medyan::setTime(impl, time); }

    /// Add Reaction *r to the chemical network which needs to be simulated
    void addReaction(ReactionBase *r) { medyan::addReaction(impl, r); }
    
    /// Remove Reaction *r from the simulated chemical network 
    void removeReaction(ReactionBase *r) { medyan::removeReaction(impl, r); }
    
    bool run(FP& curtime, FP time, DissipationTracker* pdt=nullptr, PerformanceStats* pstats=nullptr) {
        return medyan::run(impl, curtime, time, pdt, pstats);
    }
    
    bool runSteps(FP& curtime, int steps, DissipationTracker* pdt=nullptr, PerformanceStats* pstats=nullptr) {
        return medyan::runSteps(impl, curtime, steps, pdt, pstats);
    }
    
    /// Mainly used for debugging: print chemical reactions in the network at
    /// this moment
    void printReactions() const { medyan::printReactions(impl); }

    /// Cross checks all reactions in the network for firing time.
    bool crosschecktau() const { return medyan::crosschecktau(impl); }
};

} // namespace medyan

#endif

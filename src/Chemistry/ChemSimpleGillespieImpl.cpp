
//------------------------------------------------------------------
//  **MEDYAN** - Simulation Package for the Mechanochemical
//               Dynamics of Active Networks, v4.0
//
//  Copyright (2015-2018)  Papoian Lab, University of Maryland
//
//                 ALL RIGHTS RESERVED
//
//  See the MEDYAN web page for more information:
//  http://www.medyan.org
//------------------------------------------------------------------

#include "ChemSimpleGillespieImpl.h"
#include "Rand.h"

namespace medyan {

void ChemSimpleGillespieImpl::initialize() {
}


ChemSimpleGillespieImpl::~ChemSimpleGillespieImpl() noexcept {
    _reactions.clear();
}

floatingpoint ChemSimpleGillespieImpl::generateTau(floatingpoint a){
    #ifdef DEBUGCONSTANTSEED
    Rand::chemistrycounter++;
    #endif
    return medyan::rand::safeExpDist(_exp_distr, a, Rand::eng);
}

floatingpoint ChemSimpleGillespieImpl::generateUniform(){
    #ifdef DEBUGCONSTANTSEED
    Rand::chemistrycounter++;
    #endif
    return _uniform_distr(Rand::eng);
}

floatingpoint ChemSimpleGillespieImpl::computeTotalA(){
    floatingpoint rates_sum = 0;
    for (auto &r : _reactions){
        rates_sum+=r->computePropensity();
    }
    return rates_sum;
}


void ChemSimpleGillespieImpl::addReaction(ReactionBase *r) {
    auto vit = find(_reactions.begin(), _reactions.end(), r);
    if(vit==_reactions.end())
        _reactions.push_back(r);
}

void ChemSimpleGillespieImpl::removeReaction(ReactionBase *r) {
    auto vit = find(_reactions.begin(), _reactions.end(), r);
    if(vit!=_reactions.end())
        _reactions.erase(vit);
}

void ChemSimpleGillespieImpl::printReactions() const {
    for (auto &r : _reactions)
        cout << (*r);
}

} // namespace medyan

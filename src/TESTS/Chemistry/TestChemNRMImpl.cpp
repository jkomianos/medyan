

#include <catch2/catch.hpp>

#include "Chemistry/ChemSim.h"
#include "Chemistry/ReactionDy.hpp"

namespace medyan {

TEST_CASE("ChemNRMImpl tests", "[ChemSim]") {

    Species a{"A", 0, 1000000, SpeciesType::unspecified, RSpeciesType::REG};
    Species b{"B", 0, 1000000, SpeciesType::unspecified, RSpeciesType::REG};
    Species c{"C", 0, 1000000, SpeciesType::unspecified, RSpeciesType::REG};
    vector<Species*> reactants{ &a, &b };
    vector<Species*> products{ &c };
    ReactionDy reaction{reactants, products, ReactionType::REGULAR, 1.0};
    ChemNRMImpl sim{};
    sim.addReaction(&reaction);
    sim.initialize();
    FP gtime = 0;
    //sim.printReactions();
    
    SECTION("Test runSteps") {
        a.up();
        b.up();
        b.up();
        //sim.initialize() must be called to refresh propensities 
        //after any copy number changes externally
        sim.initialize(); 
        REQUIRE(runSteps(sim, gtime, 1));
        REQUIRE(a.getN()==0);
        REQUIRE(b.getN()==1);
        REQUIRE(c.getN()==1);
        REQUIRE(sim.getTime()>0);
        REQUIRE(sim.getTime() == gtime);
        REQUIRE(runSteps(sim, gtime, 1)==false);
    }

    SECTION("Test run") {
        REQUIRE(run(sim, gtime, 1.0));
        REQUIRE(c.getN()==0);
        REQUIRE(a.getN()==0);
        REQUIRE(b.getN()==0);
        REQUIRE(sim.getTime() == Approx(1.0));
        REQUIRE(sim.getTime() == gtime);
        a.up();
        b.up();
        sim.initialize();
        REQUIRE(run(sim, gtime, 100.0));
        REQUIRE(sim.getTime() == Approx(101.0));
        REQUIRE(sim.getTime() == gtime);
        REQUIRE(a.getN()==0);
        REQUIRE(b.getN()==0);
        REQUIRE(c.getN()==1);
    }

}

} // namespace medyan

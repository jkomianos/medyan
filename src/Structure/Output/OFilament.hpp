#ifndef MEDYAN_Structure_Output_OFilament_hpp
#define MEDYAN_Structure_Output_OFilament_hpp

#include "Structure/OutputStruct.hpp"
#include "Structure/SubSystem.h"
#include "Util/Io/H5.hpp"

namespace medyan {

// Filament meta.
inline void extract(OutputStructFilamentMeta& outFilMeta, FilamentModel globalFilamentModel) {
    outFilMeta.globalFilamentModel = globalFilamentModel;
}
inline void write(h5::Group& grpFilMeta, const OutputStructFilamentMeta& outFilMeta) {
    const auto globalFilamentModelStr = toString(outFilMeta.globalFilamentModel);
    h5::writeDataSet(grpFilMeta, "globalFilamentModel", globalFilamentModelStr);
}
inline void read(OutputStructFilamentMeta& outFilMeta, const h5::Group& grpFilMeta) {
    auto globalFilamentModelStr = h5::readDataSet<std::string>(grpFilMeta, "globalFilamentModel");
    parse(outFilMeta.globalFilamentModel, globalFilamentModelStr);
}


inline void extract(OutputStructFilament& outFil, const SimulConfig& conf, const Filament& fil) {
    auto& cylvec = fil.getCylinderVector();
    const int firstmid = (*cylvec[0]->getFirstBead()).monomerSerial;
    const int lastmid = (*cylvec.back()->getSecondBead()).monomerSerial - 1;
    const int numMonomers = lastmid - firstmid + 1;

    outFil.id = fil.getId();
    outFil.type = fil.getType();
    outFil.numBeads = cylvec.size() + 1;
    outFil.deltaMinusEnd = fil.getDeltaMinusEnd();
    outFil.deltaPlusEnd = fil.getDeltaPlusEnd();
    if(conf.mechParams.globalFilamentModel == FilamentModel::beadCylinder) {
        outFil.beadCoords.resize(3, outFil.numBeads);
        outFil.monomerIds.resize(1, outFil.numBeads-1);
        outFil.monomerStates.resize(1, numMonomers);
        int monomeridx_tracker = 0;
        const auto addBeadCoord = [&](const Bead& b, int bi) {
            for(int dim = 0; dim < 3; ++dim) {
                outFil.beadCoords(dim, bi) = b.coordinate()[dim];
            }
        };
        for(int ci = 0; ci < cylvec.size(); ++ci) {
            auto& b1 = *cylvec[ci]->getFirstBead();
            auto& b2 = *cylvec[ci]->getSecondBead();
            outFil.monomerIds(ci) = b1.monomerSerial;
            addBeadCoord(b1, ci);
            //get monomer state
            int maxnumMonomers = conf.geoParams.cylinderNumMon[outFil.type];
            CCylinder* ccyl = cylvec[ci]->getCCylinder();
            const auto mrangeBegin = fil.monomerIndexOnCylinder(cylvec[ci]->getPosition(), b1.monomerSerial, conf);
            const auto mrangeEnd   = fil.monomerIndexOnCylinder(cylvec[ci]->getPosition(), b2.monomerSerial, conf);
            for(int midx = mrangeBegin; midx < mrangeEnd; midx++){
                CMonomer* cmonomer = ccyl->getCMonomer(midx);
                assert(cmonomer != nullptr);
                for(int i = 0; i < cmonomer->getSpeciesFilament().size(); i++) {
                    Species* s = cmonomer->getSpeciesFilament()[i];
                    assert(s != nullptr);
                    if(areEqual(s->getN(), 1.0)) {
                        // in MEDYAN.jl monomer states start at 1, 0 is reserved.
                        outFil.monomerStates(monomeridx_tracker) = i+1;
                        monomeridx_tracker += 1;
                        // Assuming any other species has copy number 0.
                        break;
                    }
                }
            }
        }
        assert(monomeridx_tracker == numMonomers);
        addBeadCoord(*cylvec.back()->getSecondBead(), cylvec.size());
    }
}
inline void extract(std::vector<OutputStructFilament>& outFils, const SimulConfig& conf, const SubSystem& sys) {
    outFils.clear();
    outFils.reserve(Filament::getFilaments().size());
    for(auto pf : Filament::getFilaments()) {
        OutputStructFilament outFil;
        extract(outFil, conf, *pf);
        outFils.push_back(std::move(outFil));
    }
}

inline void write(h5::Group& grpFils, const OutputStructFilament& outFil, int filIndex) {
    using namespace h5;
    // Write a filament.
    //----------------------------------
    h5::Group grpFil = grpFils.createGroup(to_string(filIndex));
    h5::writeDataSet(grpFil, "beadCoords", outFil.beadCoords);
    h5::writeDataSet(grpFil, "monomerIds", outFil.monomerIds);
    h5::writeDataSet(grpFil, "extraCoords", outFil.extraCoords);
    h5::writeDataSet(grpFil, "monomerStates", outFil.monomerStates);
}
inline void write(h5::Group& grpSnapshot, const std::vector<OutputStructFilament>& outFils) {
    using namespace h5;

    h5::Group grpFils = grpSnapshot.createGroup("filaments");

    // Store number of filaments.
    h5::writeDataSet<std::int64_t>(grpFils, "count", outFils.size());

    // Store filament data.
    for(int i = 0; i < outFils.size(); ++i) {
        write(grpFils, outFils[i], i);
    }
}

inline void read(OutputStructFilament& outFil, const h5::Group& grpFils, int filIndex) {
    using namespace h5;
    // Read a filament.
    //----------------------------------
    h5::Group grpFil = grpFils.getGroup(to_string(filIndex));
    h5::readDataSet(outFil.beadCoords, grpFil, "beadCoords");
    h5::readDataSet(outFil.monomerIds, grpFil, "monomerIds");
    h5::readDataSet(outFil.extraCoords, grpFil, "extraCoords");
    h5::readDataSet(outFil.monomerStates, grpFil, "monomerStates");
}
inline void read(std::vector<OutputStructFilament>& outFils, const h5::Group& grpSnapshot) {
    using namespace h5;

    h5::Group grpFils = grpSnapshot.getGroup("filaments");

    // Read number of filaments.
    auto count = h5::readDataSet<std::int64_t>(grpFils, "count");

    // Read filament data.
    outFils.resize(count);
    for(int i = 0; i < count; ++i) {
        read(outFils[i], grpFils, i);
    }
}

} // namespace medyan

#endif
